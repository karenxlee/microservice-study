# import json
# import pika
# import django
# import os
# import sys
# from django.core.mail import send_mail
# from pika.exceptions import AMQPConnectionError
# import time




# sys.path.append("")
# os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
# django.setup()


# while True:
#     try:


#         def process_approval(ch, method, properties, body):
#             # print("  Received %r" % body)

#             msg_body = json.loads(body)
#             presenter_name = msg_body["presenter_name"]
#             presenter_email = msg_body["presenter_email"]
#             title = msg_body["title"]

#             send_mail(
#                 "Your presentation has been accepted",
#                 f"{presenter_name}, we're happy to tell you that your presentation {title} has been accepted",
#                 "admin@conference.go",
#                 [presenter_email],
#                 fail_silently=False,
#                 )


#         def process_rejection(ch, method, properties, body):
#             # print("  Received %r" % body)

#             msg_body = json.loads(body)
#             presenter_name = msg_body["presentation.presenter_name"]
#             presenter_email = msg_body["presentation.presenter_email"]
#             title = msg_body["presentation.title"]

#             send_mail(
#                 'Your presentation has been rejected',
#                 f"{presenter_name}, we're sorry to tell you that your presentation {title} has been rejected",
#                 'admin@conference.go',
#                 [presenter_email],
#                 fail_silently=False,
#                 )


#         parameters = pika.ConnectionParameters(host='rabbitmq')
#         connection = pika.BlockingConnection(parameters)
#         channel = connection.channel()


#         channel.queue_declare(queue='presentation_approvals')
#         channel.basic_consume(
#             queue='presentation_approvals',
#             on_message_callback=process_approval,
#             auto_ack=True,
#         )

#         channel.queue_declare(queue='presentation_rejections')
#         channel.basic_consume(
#             queue='presentation_rejections',
#             on_message_callback=process_rejection,
#             auto_ack=True,
#         )
#         channel.start_consuming()

#     except AMQPConnectionError:
#         print("Could not connect to RabbitMQ")
#         time.sleep(2.0)


import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from django.core.mail import send_mail
sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()
while True:
    try:
        def process_approval(ch, method, properties, body):
            print("  Received %r" % body)
            message_body = json.loads(body)
            presenter_name = message_body['presenter_name']
            presenter_email = message_body['presenter_email']
            title = message_body['title']
            send_mail(
                    'Your presentation has been approved',
                    f"{presenter_name}, we are happy to tell you that your presentation '{title}' has been approved",
                    'admin@conference.go',
                    [presenter_email],
                    fail_silently=False,
                )
        def process_rejection(ch, method, properties, body):
            print("  Received %r" % body)
            message_body = json.loads(body)
            presenter_name = message_body['presenter_name']
            presenter_email = message_body['presenter_email']
            title = message_body['title']
            send_mail(
                'Your presentation has been rejected',
                f"{presenter_name}, we are sorry to tell you that your presentation'{title}' has been rejected",
                'admin@conference.go',
                [presenter_email],
                fail_silently=False,
            )
            # print ("message sent")
        parameters = pika.ConnectionParameters(host='rabbitmq')
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue='presentation_approvals')
        channel.basic_consume(
            queue='presentation_approvals',
            on_message_callback=process_approval,
            auto_ack=True,
        )
        channel.queue_declare(queue='presentation_rejections')
        channel.basic_consume(
            queue='presentation_rejections',
            on_message_callback=process_rejection,
            auto_ack=True,
        )
        channel.start_consuming()
        # print("start email")
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
